//REQUIRES
const http = require("http");
const app = require("./app");
const server = http.createServer(app);
const jws = require('jsonwebtoken')
require('dotenv').config();


//VARIABLES DE ENTORNO
const { API_PORT } = process.env;
const port = process.env.PORT || API_PORT;

//SERVER LISTEN
server.listen(port, () => {

  console.log(`Server running on http://localhost:${port}`);
});
