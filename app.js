//EXPRESS
const express = require("express");
const app = express();
app.use(express.json());
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const fileMiddlewares = require("./middleware/file.middleware");

//VARIABLES DE ENTORNO
require("dotenv").config();
require("./config/database").connect();

//MODELS
const User = require("./model/user");

//ROUTES

app.post(
  "/register",
  [fileMiddlewares.upload.single("image")],
  async (req, res) => {
    //REGISTER LOGIC
    try {
      // GET USER INFO
      const { first_name, last_name, email, password } = req.body;

      //CHECK IF USER IS IN DB
      const userIsInDB = await User.findOne({ email });
      if (userIsInDB)
        return res.status(409).send("User Already Exist. Please Login");

      //HASH PASSWORD
      const encryptedPassword = await bcrypt.hash(password, 10);

      //IMAGE REQ
      const avatar = req.file ? req.file.filename : null;

      // CREATE USER
      const user = await User.create({
        first_name,
        last_name,
        email: email.toLowerCase(), // sanitize: convert email to lowercase
        password: encryptedPassword,
        image: avatar,
      });

      // CREATE TOKEN
      const token =  jwt.sign(
        { user_id: user._id, email },
        process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }
      );
  console.log('this is the token ---------------->' + token)
      // SAVE TOKEN
      user.token = token;

      // RETURN NEW USER
      res.status(201).json(user);
    } catch (error) {
      console.log(error);
    }
    //END
  }
);


const auth = require("./middleware/auth");

app.post("/welcome", auth, (req, res) => {
  res.status(200).send("Welcome 🙌 ");
});

  app.post("/login", async (req, res) => {


    try {

      const { email, password } = req.body;

      if (!(email && password)) {
        res.status(400).send("All input is required");
      }

      const user = await User.findOne({ email });

      if (user && (await bcrypt.compare(password, user.password))) {

        const token = jwt.sign(
          { user_id: user._id, email },
          process.env.TOKEN_KEY,
          {
            expiresIn: "2h",
          }
        );
        // save user token
        user.token = token;
        // user
        res.status(200).json(user);
      }
      res.status(400).send("Invalid Credentials");
    } catch (err) {
      console.log(err);
    }
  });


module.exports = app;
