const path = require('path');
const fs = require('fs'); 
const multer = require('multer');

const ACCEPTED_FILE_EXT = ['image/png', 'image/jpg', 'image/jpeg'];
const storage = multer.diskStorage({


filename: (req, file, callback)=> {
    callback(null, `${Date.now()}-${file.originalname}`)
},
destination: (req, file, callback)=>{
    const directory = path.join(__dirname, '../public/uploads');
    callback(null, directory)
}
});

const fileFilter = ((req, file, callback)=> {

if(ACCEPTED_FILE_EXT.includes(file.mimetype)){
    callback(null, true)
}else{
    const error = new Error('invalid MIMETYPE')
    error.status = 400;
    callback(error, null)
}
});

const upload = multer ({
    storage,
    fileFilter,
})

module.exports = {upload}