const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  first_name: { type: String, default: null },
  last_name: { type: String, default: null },
  email: { type: String, },
  password: { type: String },
  image: { type: String, default: 'https://cdn.pixabay.com/photo/2013/07/13/12/07/avatar-159236_1280.png'},
  token: { type: String },
});

module.exports = mongoose.model("jwtusers", userSchema);


